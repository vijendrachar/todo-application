import { createRouter, createWebHistory } from 'vue-router';
import Home from '../views/Home.vue';
import NewTask from '../components/NewTask.vue';
import EditTask from '../components/EditTask.vue';

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/new-task',
    name: 'NewTask',
    component: NewTask,
  },
  {
    path: '/edit-task/:id',
    name: 'EditTask',
    component: EditTask,
    props: true,
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
